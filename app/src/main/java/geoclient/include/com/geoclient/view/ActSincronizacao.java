package geoclient.include.com.geoclient.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import geoclient.include.com.geoclient.R;
import geoclient.include.com.geoclient.controller.AcessoRest;

public class ActSincronizacao extends AppCompatActivity {

    CheckBox ckbReceberDados, ckbTransmitirDados;
    Button btnSincronizar;
    ProgressDialog pgDialog;
    Handler h = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_sincronizacao);

        /***Vinculando Objetos aos componentes da View***/
        ckbReceberDados = (CheckBox) findViewById(R.id.actSincronizacao_ckbReceberDados);
        ckbTransmitirDados = (CheckBox) findViewById(R.id.actSincronizacao_ckbTransmitirDados);
        btnSincronizar = (Button) findViewById(R.id.actSincronizacao_btnSincronizar);

        /*** OnClick do botao btnSincronizar ***/
        btnSincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Thread() {
                    public void run() {

                        try {
                            manipularProgressDialog("START");
                            Thread.sleep(5000);
                            manipularProgressDialog("END");
                        } catch (Exception e) {

                        }

                    }
                }.start();


            }
        });
    }

    private void manipularProgressDialog(final String a) {
        h.post(new Runnable() {
            @Override
            public void run() {
                if (a.equals("START")) {
                    pgDialog = ProgressDialog.show(ActSincronizacao.this, "Sincronizando", "Sincronizando dados, por favor aguarde!");
                } else if (a.equals("END")){
                    pgDialog.dismiss();
                }
            }
        });
    }

    private void callServer(final String method, final String data){
        new Thread(){
            public void run(){
                AcessoRest.getSetDataWeb("http://ec2-52-67-91-204.sa-east-1.compute.amazonaws.com:8080/webservice-java/webapi/visita", data);
            }
        }.start();
    }
}
