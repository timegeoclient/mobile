package geoclient.include.com.geoclient.model.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import geoclient.include.com.geoclient.model.Colaborador;
import geoclient.include.com.geoclient.model.Parametros;
import geoclient.include.com.geoclient.view.Principal;

/**
 * Created by João Filho on 17/10/2016.
 */

public class ColaboradorDAO {
    /***
     * Banco de dados a ser manipulado
     ***/
    SQLiteDatabase bancoDados;
    /***
     * Flag para o nome da tabela: <b>Colaborador</b>
     ***/
    private final String TABELA = "COLABORADOR";
    /***
     * Flags dos campos da Tabela: <b>Colaborador</b>
     ***/
    private final String[] CAMPOS = Colaborador.colunasColaborador;

    /***
     * Método construtor
     ***/
    public ColaboradorDAO(SQLiteDatabase bancoDados) {
        this.bancoDados = bancoDados;
    }

    /***
     * Método responsável por salvar um colaborador no banco de dados
     *
     * @param c
     ***/
    public void salvarColaborador(Colaborador c) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Colaborador.NOME_COLAB, c.getNome());
        contentValues.put(Colaborador.USUARIO_COLAB, c.getUsuario());
        contentValues.put(Colaborador.SENHA_COLAB, c.getSenha());
        contentValues.put(Colaborador.MAC_COLAB, c.getMacAparelho());
        contentValues.put(Colaborador.SO_COLAB, c.getSoAparelho());
        contentValues.put(Colaborador.VESAO_COLAB, c.getVesaoAparelho());
        contentValues.put(Colaborador.ID_COLAB, c.getId());
        contentValues.put(Colaborador.VALIDACAO_COLAB, c.getValidacao());
        contentValues.put(Colaborador.CHAVE_VERIFICACAO, c.getChaveVerificacao());
        contentValues.put(Colaborador.ID_EMPRESA, c.getEmpresa().getId());
        contentValues.put(Colaborador.NUMERO_COLAB, c.getNumero());
        contentValues.put(Colaborador.EMAIL_COLAB, c.getEmail());

        bancoDados.insert(TABELA, null, contentValues);
    }

    /***
     * Método responsável por retornar um Colaborador do banco de dados
     *
     * @param id_colab
     * @return
     ***/
    public Colaborador getColaborador(int id_colab) {
        Cursor cursor = Principal.conexaoBanco.getCursor(CAMPOS, TABELA,
                Colaborador.ID_COLAB + " = " + String.valueOf(id_colab), null, Colaborador.ID_COLAB);

        if (cursor.moveToFirst()) {
            Colaborador c = new Colaborador();
            c.setId(cursor.getInt(cursor.getColumnIndex(Colaborador.ID_COLAB)));
            c.setChaveVerificacao(cursor.getString(cursor.getColumnIndex(Colaborador.CHAVE_VERIFICACAO)));
            c.setEmail(cursor.getString(cursor.getColumnIndex(Colaborador.EMAIL_COLAB)));
            c.setMacAparelho(cursor.getString(cursor.getColumnIndex(Colaborador.MAC_COLAB)));
            c.setNome(cursor.getString(cursor.getColumnIndex(Colaborador.NOME_COLAB)));
            c.setNumero(cursor.getString(cursor.getColumnIndex(Colaborador.NUMERO_COLAB)));
            c.setSenha(cursor.getString(cursor.getColumnIndex(Colaborador.SENHA_COLAB)));
            c.setSoAparelho(cursor.getString(cursor.getColumnIndex(Colaborador.SO_COLAB)));
            c.setUsuario(cursor.getString(cursor.getColumnIndex(Colaborador.USUARIO_COLAB)));
            c.setValidacao(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(Colaborador.VALIDACAO_COLAB))));
            c.setVesaoAparelho(cursor.getString(cursor.getColumnIndex(Colaborador.VESAO_COLAB)));
            //c.setEmpresa(cursor.get(cursor.getColumnIndex;
            //c.setVisitas(cursor.get(cursor.getColumnIndex;

            return c;
        }
        return null;
    }

}
