package geoclient.include.com.geoclient.controller;

import android.annotation.SuppressLint;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;

public class AcessoRest {

    public static String getSetDataWeb(String url, String data) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        String answer = "";

        try {
            ArrayList<NameValuePair> valores = new ArrayList<NameValuePair>();
            valores.add(new BasicNameValuePair("json", data));

            httpPost.setEntity(new UrlEncodedFormEntity(valores));
            HttpResponse resposta = httpClient.execute(httpPost);
            answer = EntityUtils.toString(resposta.getEntity());
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return (answer);
    }

    @SuppressLint("NewApi")
    public static String callServer(final String data) {
        String answer = "";
        try {
            new Thread() {
                public void run() {
                    String answer = AcessoRest.getSetDataWeb("http://ec2-52-67-91-204.sa-east-1.compute.amazonaws.com:8080/webservice-java/webapi/visita", data);
                }
            }.start();


        } catch (Exception e) {

        }

        if (!answer.equals("")) {
            return answer;
        } else {
            return "";
        }
    }
}