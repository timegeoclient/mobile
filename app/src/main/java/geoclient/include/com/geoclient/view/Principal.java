package geoclient.include.com.geoclient.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import geoclient.include.com.geoclient.R;
import geoclient.include.com.geoclient.controller.AcessoRest;
import geoclient.include.com.geoclient.controller.ConexaoBanco;
import geoclient.include.com.geoclient.controller.Utilitarios;

public class Principal extends AppCompatActivity {

    EditText edtUsuario, edtSenha;
    CheckBox ckbSalvarSenha;
    Button btnEntrar;
    public static ConexaoBanco conexaoBanco;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        conexaoBanco = new ConexaoBanco(Principal.this);

        /***Vinculando Objetos aos componentes da View***/
        edtUsuario = (EditText) findViewById(R.id.actLogin_EdtUsuario);
        edtSenha = (EditText) findViewById(R.id.actLogin_EdtSenha);
        ckbSalvarSenha = (CheckBox) findViewById(R.id.actLogin_CkbSalvarDados);
        btnEntrar = (Button) findViewById(R.id.actLogin_BtnEntrar);

        /*** OnClick do botão btnEntrar ***/
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /***
                 * Capturando valores dos EditTexts
                 * ***/
                String user = edtUsuario.getText().toString();
                String pass = edtSenha.getText().toString();

                /***
                 * Edits não estarem vazias
                 * ***/
                if (!user.equals("") && !pass.equals("")) {

                    /***
                     *  Transformando os textos em json eobtendo resultado do servidor
                     *  ***/
                    String json = verificaUsuario(user, pass);
                    String answer = AcessoRest.callServer(json);


                    /***
                     *  Usuario e senha existentes
                     *  ***/
                    if (!answer.equals("")) {

                        Utilitarios.showToast(answer, Principal.this);
                        Intent i = new Intent(Principal.this, ActMenuPrincipal.class);
                        startActivity(i);

                    } else {
                        Toast.makeText(Principal.this, "Usuário ou senha invalidos!", Toast.LENGTH_SHORT).show();
                        //Utilitarios.showToast(AcessoRest.callServer(verificaUsuario("user", "pass")), Principal.this);
                    }

                } else {
                    Toast.makeText(Principal.this, "Por favor, insira um usuário e senha!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public String verificaUsuario(String usuario, String senha) {
        JSONObject jo = new JSONObject();
        try {
            jo.put("senha", senha);
            jo.put("usuario", usuario);
        } catch (JSONException je) {

        }
        return jo.toString();
    }
}
