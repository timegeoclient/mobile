package geoclient.include.com.geoclient.model;

import android.provider.BaseColumns;

import java.io.Serializable;
import java.util.Date;

public class Visitas implements Serializable, BaseColumns {
    /***
     * Campos da tabela Visitas
     ***/
    public static final String ID_COLAB = "ID_COLAB";
    public static final String ID_LOCA = "ID_LOCA";
    public static final String ID_VISITA = "ID_VISITA";
    public static final String LATITUDE_VISI = "LATITUDE_VISI";
    public static final String LONGITUDE_VISI = "LONGITUDE_VISI";
    public static final String DATA_ATRIBUIDA = "DATA_ATRIBUIDA";
    public static final String DATA_RETORN = "DATA_RETORN";
    public static final String DATA_APROV = "DATA_APROV";
    public static final String DATA_REJEICAO = "DATA_REJEICAO";
    public static final String PRECISAO_VIS = "PRECISAO_VIS";
    public static final String STELITESATIVOS_VIS = "STELITESATIVOS_VIS";

    /***
     * Vetor para armazenar os campos da tabela
     ***/
    public static String[] colunasVisitas = new String[]{
            Visitas.ID_COLAB,
            Visitas.ID_LOCA,
            Visitas.ID_VISITA,
            Visitas.LATITUDE_VISI,
            Visitas.LONGITUDE_VISI,
            Visitas.DATA_ATRIBUIDA,
            Visitas.DATA_RETORN,
            Visitas.DATA_APROV,
            Visitas.DATA_REJEICAO,
            Visitas.PRECISAO_VIS,
            Visitas.STELITESATIVOS_VIS
    };


    public enum EnumVisitaFeita {
        SIM("SIM"),
        NAO("NÃO");
        String tipo;

        private EnumVisitaFeita(String tipo) {
            this.tipo = tipo;
        }

        public String getTipo() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public static boolean isTipoValido(String tipo) {
            String tipoMaiusculo = tipo.toUpperCase();
            return tipoMaiusculo.equals(EnumVisitaFeita.NAO.getTipo()) ||
                    tipoMaiusculo.equals(EnumVisitaFeita.SIM.getTipo());
        }

    }

    public enum EnumVisitaStatus {
        PENDENTE("PENDENTE"),
        REALIZADA("REALIZADA"),
        CANCELADA("CANCELADA");
        private String tipo;

        private EnumVisitaStatus(String tipo) {
            this.tipo = tipo;
        }

        public String getTipo() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public static boolean isTipoValido(String tipo) {
            String tipoMaiusculo = tipo.toUpperCase();
            return tipoMaiusculo.equals(EnumVisitaStatus.CANCELADA.getTipo()) ||
                    tipoMaiusculo.equals(EnumVisitaStatus.PENDENTE.getTipo()) ||
                    tipoMaiusculo.equals(EnumVisitaStatus.REALIZADA.getTipo());
        }

    }

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String bairro;
    private String cidade;
    private Date dataAprovacao;
    private Date dataAtribuicao;
    private Date dataRetorno;
    private Date dataRejeicao;
    private String latitude;
    private String logradouro;
    private String longitude;
    private String numero;
    private String precisao;
    private String rua;
    private Integer numeroSatelitesAtivos;
    private String status;
    private String feita;
    private Colaborador colaborador;
    private Localizacao localizacao;

    public Visitas() {
    }

    public Visitas(Integer id, String bairro, String cidade, Date dataAprovacao, Date dataAtribuicao, Date dataRetorno,
                   Date dataRejeicao, String latitude, String logradouro, String longitude, String numero, String precisao,
                   String rua, Integer numeroSatelitesAtivos, String status, String feita) {
        this(id, bairro, cidade, dataAprovacao, dataAtribuicao, dataRetorno, dataRejeicao, latitude, logradouro, longitude,
                numero, precisao, rua, numeroSatelitesAtivos, status, feita, null, null);
    }

    public Visitas(String bairro, String cidade, Date dataAprovacao, Date dataAtribuicao, Date dataRetorno,
                   Date dataRejeicao, String latitude, String logradouro, String longitude, String numero, String precisao,
                   String rua, Integer numeroSatelitesAtivos, String status, String feita, Colaborador colaborador,
                   Localizacao localizacao) {
        this(null, bairro, cidade, dataAprovacao, dataAtribuicao, dataRetorno, dataRejeicao, latitude, logradouro, longitude,
                numero, precisao, rua, numeroSatelitesAtivos, status, feita, colaborador, localizacao);
    }

    public Visitas(Integer id, String bairro, String cidade, Date dataAprovacao, Date dataAtribuicao, Date dataRetorno,
                   Date dataRejeicao, String latitude, String logradouro, String longitude, String numero, String precisao,
                   String rua, Integer numeroSatelitesAtivos, String status, String feita, Colaborador colaborador,
                   Localizacao localizacao) {
        super();
        this.setId(id);
        this.setBairro(bairro);
        this.setCidade(cidade);
        this.setDataAprovacao(dataAprovacao);
        this.setDataAtribuicao(dataAtribuicao);
        this.setDataRetorno(dataRetorno);
        this.setDataRejeicao(dataRejeicao);
        this.setLatitude(latitude);
        this.setLogradouro(logradouro);
        this.setLongitude(longitude);
        this.setNumero(numero);
        this.setPrecisao(precisao);
        this.setRua(rua);
        this.setNumeroSatelitesAtivos(numeroSatelitesAtivos);
        this.setStatus(status);
        this.setFeita(feita);
        this.setColaborador(colaborador);
        this.setLocalizacao(localizacao);
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBairro() {
        return this.bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return this.cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public Date getDataAprovacao() {
        return this.dataAprovacao;
    }

    public void setDataAprovacao(Date dataAprovacao) {
        this.dataAprovacao = dataAprovacao;
    }

    public Date getDataAtribuicao() {
        return this.dataAtribuicao;
    }

    public void setDataAtribuicao(Date dataAtribuicao) {
        this.dataAtribuicao = dataAtribuicao;
    }

    public Date getDataRetorno() {
        return this.dataRetorno;
    }

    public void setDataRetorno(Date dataRetorno) {
        this.dataRetorno = dataRetorno;
    }

    public Date getDataRejeicao() {
        return this.dataRejeicao;
    }

    public void setDataRejeicao(Date dataRejeicao) {
        this.dataRejeicao = dataRejeicao;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLogradouro() {
        return this.logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getPrecisao() {
        return this.precisao;
    }

    public void setPrecisao(String precisao) {
        this.precisao = precisao;
    }

    public String getRua() {
        return this.rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public Integer getNumeroSatelitesAtivos() {
        return this.numeroSatelitesAtivos;
    }

    public void setNumeroSatelitesAtivos(Integer numeroSatelitesAtivos) {
        this.numeroSatelitesAtivos = numeroSatelitesAtivos;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        String statusMaiusculo = status.toUpperCase();
        if (EnumVisitaStatus.isTipoValido(statusMaiusculo))
            this.status = statusMaiusculo;
        else
            throw new IllegalArgumentException("O paramêtro 'status' deve ter um dos valores do Enum 'EnumVisitaStatus'.");
    }

    public String getFeita() {
        return this.feita;
    }

    public void setFeita(String feita) {
        String feitaMaiuscula = feita.toUpperCase();
        if (EnumVisitaFeita.isTipoValido(feita))
            this.feita = feitaMaiuscula;
        else
            throw new IllegalArgumentException("O paramêtro 'visita feita' deve ter um dos valores do Enum 'EnumVisitaFeita'.");
    }

    public Colaborador getColaborador() {
        return this.colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public Localizacao getLocalizacao() {
        return this.localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

}