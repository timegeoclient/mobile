package geoclient.include.com.geoclient.view;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;

import geoclient.include.com.geoclient.model.Colaborador;
import geoclient.include.com.geoclient.model.Localizacao;
import geoclient.include.com.geoclient.model.Visitas;
import geoclient.include.com.geoclient.model.VisitasListAdapter;

public class ActListarVisitas extends ListActivity {

    public ArrayList<Visitas> LVis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LVis = new ArrayList<Visitas>();

        setListAdapter(new VisitasListAdapter(this, LVis));
    }

    @Override
    protected void onListItemClick(ListView lv, View v, int posicao, long id) {
        super.onListItemClick(lv, v, posicao, id);
//        Intent i = new Intent(ActListarVisitass.this, ActDadosVisitass.class);
        Visitas e = LVis.get(posicao);
//        String s[] = new String[]{String.valueOf(e.getId_visita()),
//                String.valueOf(e.getId_colab()),
//                String.valueOf(e.getId_loca()),
//                e.getLatitude_visi(),
//                e.getLongitude_visi(),
//                e.getPrecisao_visi(),
//                String.valueOf(e.getSatelites_ativos()),
//                e.getNome_cliente(),
//                e.getLogradouro_cliente(),
//                e.getNumero_cliente(),
//                e.getComplemento_cliente(),
//                e.getBairro_cliente(),
//                e.getCep_cliente(),
//                e.getCidade_cliente(),
//                e.getEstado_cliente()};

//        i.putExtra("EMPRESA", s);
        //i.putExtra("DATA_ATRIBUIDA", e.getData_atribuida());
        //i.putExtra("DATA_RETORNADA", e.getData_retornada());
//        startActivity(i);

    }
}