package geoclient.include.com.geoclient.model;

import android.provider.BaseColumns;

/**
 * Created by João Filho on 22/10/2016.
 */

public class Parametros implements BaseColumns {

    /***
     * Campos da tabela Parametros
     ***/
    public static final String COD_PAR = "COD_PAR";
    public static final String USUARIO_SALVO = "USUARIO_SALVO";
    public static final String SENHA_SALVA = "SENHA_SALVA";

    /***
     * Vetor para armazenar os campos da tabela
     ***/
    public static String[] colunasParametros = new String[]{
            Parametros.COD_PAR,
            Parametros.USUARIO_SALVO,
            Parametros.SENHA_SALVA
    };

    /*** Variáveis de instância ***/
    private int cod_par;
    private String usuario_salvo;
    private String senha_salva;

    public Parametros() {
    }

    public Parametros(int cod_par, String usuario_salvo, String senha_salva) {
        this.cod_par = cod_par;
        this.usuario_salvo = usuario_salvo;
        this.senha_salva = senha_salva;
    }

    public int getCod_par() {
        return cod_par;
    }

    public void setCod_par(int cod_par) {
        this.cod_par = cod_par;
    }

    public String getUsuario_salvo() {
        return usuario_salvo;
    }

    public void setUsuario_salvo(String usuario_salvo) {
        this.usuario_salvo = usuario_salvo;
    }

    public String getSenha_salva() {
        return senha_salva;
    }

    public void setSenha_salva(String senha_salva) {
        this.senha_salva = senha_salva;
    }
}
