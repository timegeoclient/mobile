package geoclient.include.com.geoclient.model.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import geoclient.include.com.geoclient.model.Localizacao;
import geoclient.include.com.geoclient.model.Visitas;
import geoclient.include.com.geoclient.view.Principal;

/**
 * Created by João Filho on 17/10/2016.
 */

public class VisitasDAO {
    /***
     * Banco de dados a ser manipulado
     ***/
    SQLiteDatabase bancoDados;
    /***
     * Flag para o nome da tabela: <b>Visitas</b>
     ***/
    private final String TABELA = "VISITAS";
    /***
     * Flags dos campos da Tabela: <b>Visitas</b>
     ***/
    private final String[] CAMPOS = Visitas.colunasVisitas;

    /***
     * Método construtor
     ***/
    public VisitasDAO(SQLiteDatabase bancoDados) {
        this.bancoDados = bancoDados;
    }

    /***
     * Salvar visita no banco de dados
     *
     * @param visita
     ***/
    public void salvarVisita(Visitas visita) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Visitas.ID_COLAB, visita.getColaborador().getId());
        contentValues.put(Visitas.ID_LOCA, visita.getLocalizacao().getId());
        contentValues.put(Visitas.ID_VISITA, visita.getId());
        contentValues.put(Visitas.LATITUDE_VISI, visita.getLatitude());
        contentValues.put(Visitas.LONGITUDE_VISI, visita.getLongitude());
        contentValues.put(Visitas.DATA_ATRIBUIDA, visita.getDataAtribuicao().toString());
        contentValues.put(Visitas.DATA_RETORN, visita.getDataRetorno().toString());
        contentValues.put(Visitas.DATA_APROV, visita.getDataAprovacao().toString());
        contentValues.put(Visitas.DATA_REJEICAO, visita.getDataRejeicao().toString());
        contentValues.put(Visitas.PRECISAO_VIS, visita.getPrecisao().toString());
        contentValues.put(Visitas.STELITESATIVOS_VIS, visita.getNumeroSatelitesAtivos().toString());

        bancoDados.insert(TABELA, null, contentValues);
    }

    /***
     * Listar Visitas do banco de dados
     *
     * @param where
     * @param groupBy
     * @param orderBy
     * @return
     ***/
    public List<Visitas> listarVisitas(String where, String groupBy, String orderBy) {
        Cursor cursor = Principal.conexaoBanco.getCursor(CAMPOS, TABELA, where, groupBy, orderBy);

        List<Visitas> LVisitas = new ArrayList<Visitas>();

        if (cursor.moveToFirst()) {
            do {

                Visitas v = new Visitas();
                ColaboradorDAO CDAO = new ColaboradorDAO(this.bancoDados);
                LocalizacaoDAO LDAO = new LocalizacaoDAO(this.bancoDados);
                Localizacao l = LDAO.getLocalizacao(cursor.getInt(cursor.getColumnIndex(Visitas.ID_LOCA)));

                v.setId(cursor.getInt(cursor.getColumnIndex(Visitas.ID_VISITA)));
                v.setLocalizacao(l);
                v.setBairro(l.getBairro());
                v.setCidade(l.getCidade());
                v.setDataAprovacao(Date.valueOf(cursor.getString(cursor.getColumnIndex(Visitas.DATA_APROV))));
                v.setDataAtribuicao(Date.valueOf(cursor.getString(cursor.getColumnIndex(Visitas.DATA_ATRIBUIDA))));
                v.setDataRetorno(Date.valueOf(cursor.getString(cursor.getColumnIndex(Visitas.ID_COLAB))));
                v.setDataRejeicao(Date.valueOf(cursor.getString(cursor.getColumnIndex(Visitas.ID_COLAB))));
                v.setLatitude(cursor.getString(cursor.getColumnIndex(Visitas.LATITUDE_VISI)));
                v.setLogradouro(l.getLogradouro());
                v.setLongitude(cursor.getString(cursor.getColumnIndex(Visitas.LONGITUDE_VISI)));
                v.setNumero(l.getNumero());
                v.setPrecisao(cursor.getString(cursor.getColumnIndex(Visitas.PRECISAO_VIS)));
                v.setRua(l.getRua());
                v.setNumeroSatelitesAtivos(cursor.getInt(cursor.getColumnIndex(Visitas.STELITESATIVOS_VIS)));
                v.setStatus(l.getStatus());
                //v.setFeita();
                v.setColaborador(CDAO.getColaborador(cursor.getInt(cursor.getColumnIndex(Visitas.ID_COLAB))));

                LVisitas.add(v);

            } while (cursor.moveToNext());
        }

        return LVisitas;
    }
}
