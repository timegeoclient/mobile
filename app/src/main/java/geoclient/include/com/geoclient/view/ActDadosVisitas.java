package geoclient.include.com.geoclient.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;

import org.w3c.dom.Text;

import java.util.GregorianCalendar;

import geoclient.include.com.geoclient.R;
import geoclient.include.com.geoclient.controller.GPSTracker;
import geoclient.include.com.geoclient.controller.Utilitarios;
import geoclient.include.com.geoclient.model.Visitas;
import geoclient.include.com.geoclient.model.Visitas;

public class ActDadosVisitas extends AppCompatActivity {

    TextView txtIdVisita, txtNomeCliente, txtEndereco, txtDataAtribuida, txtDataRetornada, txtPrecisao, txtSatelites, txtLatitude, txtLongitude;
    Button btnAtualizarLatLong;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_dados_visitas);

        /***Vinculando Objetos aos componentes da View***/
        txtIdVisita = (TextView) findViewById(R.id.actDadosVisita_TxtIdVisita);
        txtNomeCliente = (TextView) findViewById(R.id.actDadosVisita_TxtNomeCliente);
        txtEndereco = (TextView) findViewById(R.id.actDadosVisita_TxtEndereco);
        txtDataAtribuida = (TextView) findViewById(R.id.actDadosVisita_TxtDataAtribuida);
        txtDataRetornada = (TextView) findViewById(R.id.actDadosVisita_TxtDataRetornada);
        txtPrecisao = (TextView) findViewById(R.id.actDadosVisita_TxtPrecisao);
        txtSatelites = (TextView) findViewById(R.id.actDadosVisita_TxtSatelitesAtivos);
        txtLatitude = (TextView) findViewById(R.id.actDadosVisita_TxtLatitude);
        txtLongitude = (TextView) findViewById(R.id.actDadosVisita_TxtLongitude);
        btnAtualizarLatLong = (Button)findViewById(R.id.actDadosVisita_BtnAtualizarLatLong);




        /*** Ler dados recebidos da intent para pegar os dados da Visita ***/
        Intent i = getIntent();
        if (i != null) {
            String[] s = i.getStringArrayExtra("EMPRESA");
            //GregorianCalendar dataAtribuida = i.getParcelableExtra("DATA_ATRIBUIDA");
            //GregorianCalendar dataRetornada = i.getParcelableExtra("DATA_RETORNADA");
//            Visitas v = new Visitas()
//
//            txtIdVisita.setText(String.valueOf(v.getId_visita()));
//            txtNomeCliente.setText(v.getNome_cliente());
//            txtEndereco.setText(v.getLogradouro_cliente() + ", " + v.getNumero_cliente() + ", " + v.getComplemento_cliente() + ", " + v.getBairro_cliente() + ", " + v.getCidade_cliente() + ", " + v.getEstado_cliente());
//            txtDataAtribuida.setText(Utilitarios.getDataPorExtenso(v.getData_atribuida().getTime()));
//            txtDataRetornada.setText(Utilitarios.getDataPorExtenso(v.getData_retornada().getTime()));
//            txtPrecisao.setText(v.getPrecisao_visi());
//            txtSatelites.setText(String.valueOf(v.getSatelites_ativos()));
//            txtLatitude.setText(v.getLatitude_visi());
//            txtLongitude.setText(v.getLongitude_visi());

        }
        btnAtualizarLatLong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GPSTracker gps = new GPSTracker(ActDadosVisitas.this);

                // checa se o GPS está habilitado
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    txtLatitude.setText(String.valueOf(gps.getLatitude()));
                    txtLongitude.setText(String.valueOf(gps.getLongitude()));

                } else {
                    gps.showSettingsAlert();
                }
            }
        });

    }


}
