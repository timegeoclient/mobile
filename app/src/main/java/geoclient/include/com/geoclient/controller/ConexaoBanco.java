package geoclient.include.com.geoclient.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import geoclient.include.com.geoclient.model.Parametros;

public class ConexaoBanco {

    /***
     * Váriaveis Usadas Na Classe
     ***/
    protected SQLiteDatabase bancoDados = null;
    private String sql;
    private Cursor cursor;

    public ConexaoBanco(Context contexto) {

        /*** Abre ou Cria banco de dados SqLite ***/
        bancoDados = contexto.openOrCreateDatabase("Banco", Context.MODE_PRIVATE, null);

        /*** Cria as tabelas do banco ***/
        sql = " CREATE TABLE IF NOT EXISTS colaborador (" +
                "nome_colab character varying (200) NOT NULL," +
                "usuario_colab character varying(20) NOT NULL," +
                "senha_colab character varying(32) NOT NULL," +
                "mac_colab character varying(17) NOT NULL," +
                "so_colab character varying(30) NOT NULL," +
                "vesao_colab character varying(10) NOT NULL," +
                "id_colab integer NOT NULL primary key," +
                "validacao_colab boolean NOT NULL DEFAULT false," +
                "chave_verificacao character varying (40)," +
                "id_empresa integer NOT NULL," +
                "numero_colab character varying (20) NOT NULL," +
                "email_colab character varying(150) NOT NULL" +
                ");";
        bancoDados.execSQL(sql);

        sql = "CREATE TABLE IF NOT EXISTS localizacao (" +
                "id_loca integer NOT NULL primary key," +
                "satelites_ativos integer," +
                "precisao_loca character varying (20)," +
                "nome_cliente character varying (200) NOT NULL," +
                "longitude_cliente character varying(20)," +
                "latitude_cliente character varying (20)," +
                "logradoro_cliente character varying (250)," +
                "numero_cliente character varying (8) NOT NULL," +
                "complemento_cliente character varying(100)," +
                "bairro_cliente character varying (100) NOT NULL," +
                "cep_cliente character varying(9) NOT NULL," +
                "cidade_cliente character varying(100) NOT NULL," +
                "estado_cliente character varying(2) NOT NULL," +
                "id_empresa integer," +
                "id_colab integer," +
                "status_visita character varying(50)" +
                ");";
        bancoDados.execSQL(sql);

        sql = " CREATE TABLE IF NOT EXISTS visitas (" +
                "id_colab integer NOT NULL," +
                "id_loca integer NOT NULL," +
                "id_visita integer NOT NULL primary key," +
                "latitude_visi character varying (20)," +
                "longitude_visi character varying (20)," +
                "data_atribuida date," +
                "data_retorn date," +
                "data_aprov date," +
                "data_rejeicao date," +
                "precisao_vis character varying (20)," +
                "stelitesativos_vis integer" +
                " );";
        bancoDados.execSQL(sql);

        sql = "CREATE TABLE IF NOT EXISTS parametros(" +
                "cod_par integer NOT NULL primary key," +
                "usuario_salvo varchar(50)," +
                "senha_salva varchar(50));";
        bancoDados.execSQL(sql);

        /*** Inserindo registros padrões ***/
        if (getCursor(Parametros.colunasParametros, "PARAMETROS",
                null, null, null).getCount() <= 0) {
            sql = "INSERT INTO PARAMETROS " +
                    "(COD_PAR, USUARIO_SALVO, SENHA_SALVA) "
                    + "VALUES " + "(1, '', '');";
            bancoDados.execSQL(sql);
        }
    }

    /***
     * Retorna dados do banco
     *
     * @param columns
     * @param table
     * @param where
     * @param groupBy
     * @param orderBy
     * @return Cursor
     ***/
    public Cursor getCursor(String[] columns, String table, String where,
                            String groupBy, String orderBy) {
        try {
            return bancoDados.query(table, columns, where, null, groupBy, null,
                    orderBy);
        } catch (SQLException E) {
            return null;
        }
    }

    /***
     * Gera um novo codigo de acordo com o último código de uma tabela
     *
     * @param campoTabela
     * @param nomeTabela
     * @return Stringl
     ***/
    public String gerarNovoCodigo(String campoTabela, String nomeTabela) {
        sql = "SELECT MAX(" + campoTabela + ") AS PRXCOD FROM " + nomeTabela;
        Cursor cursor = bancoDados.rawQuery(sql, null);

        int cod = 0;
        if (cursor.moveToFirst()) {
            do {
                cod = cursor.getInt(0) + 1;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return Integer.toString(cod);
    }
}
