package geoclient.include.com.geoclient.model;

import android.provider.BaseColumns;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Localizacao implements Serializable, BaseColumns {
    /***
     * Campos da tabela Localização
     ***/
    public static final String ID_LOCA = "ID_LOCA";
    public static final String SATELITES_ATIVOS = "SATELITES_ATIVOS";
    public static final String PRECISAO_LOCA = "PRECISAO_LOCA";
    public static final String NOME_CLIENTE = "NOME_CLIENTE";
    public static final String LONGITUDE_CLIENTE = "LONGITUDE_CLIENTE";
    public static final String LATITUDE_CLIENTE = "LATITUDE_CLIENTE";
    public static final String LOGRADORO_CLIENTE = "LOGRADORO_CLIENTE";
    public static final String NUMERO_CLIENTE = "NUMERO_CLIENTE";
    public static final String COMPLEMENTO_CLIENTE = "COMPLEMENTO_CLIENTE";
    public static final String BAIRRO_CLIENTE = "BAIRRO_CLIENTE";
    public static final String CEP_CLIENTE = "CEP_CLIENTE";
    public static final String CIDADE_CLIENTE = "CIDADE_CLIENTE";
    public static final String ESTADO_CLIENTE = "ESTADO_CLIENTE";
    public static final String ID_EMPRESA = "ID_EMPRESA";
    public static final String ID_COLAB = "ID_COLAB";
    public static final String STATUS_VISITA = "STATUS_VISITA";

    /***
     * Vetor para armazenar os campos da tabela
     ***/
    public static String[] colunasLocalizacao = new String[]{
            Localizacao.ID_LOCA,
            Localizacao.SATELITES_ATIVOS,
            Localizacao.PRECISAO_LOCA,
            Localizacao.NOME_CLIENTE,
            Localizacao.LONGITUDE_CLIENTE,
            Localizacao.LATITUDE_CLIENTE,
            Localizacao.LOGRADORO_CLIENTE,
            Localizacao.NUMERO_CLIENTE,
            Localizacao.COMPLEMENTO_CLIENTE,
            Localizacao.BAIRRO_CLIENTE,
            Localizacao.CEP_CLIENTE,
            Localizacao.CIDADE_CLIENTE,
            Localizacao.ESTADO_CLIENTE,
            Localizacao.ID_EMPRESA,
            Localizacao.ID_COLAB,
            Localizacao.STATUS_VISITA
    };

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String bairro;
    private String cep;
    private String cidade;
    private String complemento;
    private String estado;
    private String latitude;
    private String logradouro;
    private String longitude;
    private String nome;
    private String numero;
    private String precisao;
    private String rua;
    private Integer numeroSatelitesAtivos;
    private String status;
    private Empresa empresa;
    private List<Visitas> visitas;
    private Colaborador colaborador;

    public Localizacao() {
        super();
    }

    public Localizacao(String bairro, String cep, String cidade, String complemento, String estado, String latitude,
                       String logradouro, String longitude, String nome, String numero, String precisao, String rua,
                       Integer numeroSatelitesAtivos, String status, Empresa empresa, List<Visitas> visitas,
                       Colaborador colaborador) {
        this(null, bairro, cep, cidade, complemento, estado, latitude, logradouro, longitude, nome, numero, precisao, rua, numeroSatelitesAtivos, status, empresa, visitas, colaborador);
    }

    public Localizacao(String bairro, String cep, String cidade, String complemento, String estado,
                       String latitude, String logradouro, String longitude, String nome, String numero, String precisao,
                       String rua, Integer numeroSatelitesAtivos, String status, Empresa empresa, Colaborador colaborador) {
        this(null, bairro, cep, cidade, complemento, estado, latitude, logradouro, longitude, nome, numero, precisao, rua, numeroSatelitesAtivos, status, empresa, new ArrayList<Visitas>(0), colaborador);
    }

    public Localizacao(Integer id, String bairro, String cep, String cidade, String complemento, String estado,
                       String latitude, String logradouro, String longitude, String nome, String numero, String precisao,
                       String rua, Integer numeroSatelitesAtivos, String status, Empresa empresa, List<Visitas> visitas,
                       Colaborador colaborador) {
        super();
        this.setId(id);
        this.setBairro(bairro);
        this.setCep(cep);
        this.setCidade(cidade);
        this.setComplemento(complemento);
        this.setEstado(estado);
        this.setLatitude(latitude);
        this.setLogradouro(logradouro);
        this.setLongitude(longitude);
        this.setNome(nome);
        this.setNumero(numero);
        this.setPrecisao(precisao);
        this.setRua(rua);
        this.setNumeroSatelitesAtivos(numeroSatelitesAtivos);
        this.setStatus(status);
        this.setEmpresa(empresa);
        this.setVisitas(visitas);
        this.setColaborador(colaborador);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradoro) {
        this.logradouro = logradoro;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getPrecisao() {
        return precisao;
    }

    public void setPrecisao(String precisao) {
        this.precisao = precisao;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public Integer getNumeroSatelitesAtivos() {
        return numeroSatelitesAtivos;
    }

    public void setNumeroSatelitesAtivos(Integer numeroSatelitesAtivos) {
        this.numeroSatelitesAtivos = numeroSatelitesAtivos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        String statusMaiusculo = status.toUpperCase();
        if (Visitas.EnumVisitaStatus.isTipoValido(statusMaiusculo))
            this.status = statusMaiusculo;
        else
            throw new IllegalArgumentException("O paramêtro 'status' deve ter um dos valores do Enum 'EnumVisitaStatus'.");
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Visitas> getVisitas() {
        return visitas;
    }

    public void setVisitas(List<Visitas> visitas) {
        this.visitas = visitas;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

}