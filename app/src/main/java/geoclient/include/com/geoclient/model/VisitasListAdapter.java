package geoclient.include.com.geoclient.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import geoclient.include.com.geoclient.R;

/**
 * Created by João Filho on 17/10/2016.
 */

public class VisitasListAdapter extends BaseAdapter {
    private Context contexto;
    private List<Visitas> LVisitas;

    public VisitasListAdapter(Context contexto, List<Visitas> LVisitas) {
        this.contexto = contexto;
        this.LVisitas = LVisitas;
    }

    @Override
    public int getCount() {
        return LVisitas.size();
    }

    @Override
    public Object getItem(int position) {
        return LVisitas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*** Objeto do tipo Visita usado para pegar o item na posição atual ***/
        Visitas visita = LVisitas.get(position);

        /*** LayoutInflater usado para pegar o layout onde será listado os itens ***/
        LayoutInflater layoutInflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        /*** Criado um view referenciando o layout onde será listado os itens ***/
        View view = layoutInflater.inflate(R.layout.activity_act_listar_visitas, null);

        /*** Variáveis do componentes da activity_listar_visitas ***/
        TextView txtId = (TextView) view.findViewById(R.id.actListarVisitas_TxtIdVIsita);
        TextView txtNome = (TextView) view.findViewById(R.id.actListarVisitas_TxtNomeCliente);
        TextView txtDataAtribuida = (TextView) view.findViewById(R.id.actListarVisitas_TxtDataAtribuida);
        TextView txtEndereco = (TextView) view.findViewById(R.id.actListarVisitas_TxtEndereco);
        TextView txtLatitude = (TextView) view.findViewById(R.id.actListarVisitas_TxtLatitude);
        TextView txtLongitude = (TextView) view.findViewById(R.id.actListarVisitas_TxtLongitude);

        /*** Passando o texto para os TextViews ***/
        txtId.setText(visita.getId().toString());
        txtNome.setText(visita.getLogradouro());
        txtDataAtribuida.setText(visita.getDataAtribuicao().toString());
        txtEndereco.setText(visita.getRua() + ", " + visita.getNumero() + ", " + visita.getBairro() + ", " + visita.getCidade());
        txtLatitude.setText(visita.getLatitude());
        txtLongitude.setText(visita.getLongitude());


        return view;
    }
}
