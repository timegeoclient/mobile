package geoclient.include.com.geoclient.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by João Filho on 27/07/2016.
 */
public class Utilitarios extends Activity {

    /***
     * Método responsável por mostrar um Toast
     *
     * @param texto
     * @param activity
     ***/
    public static void showToast(String texto, Activity activity) {
        Toast.makeText(activity, texto, Toast.LENGTH_SHORT).show();
    }

    /***
     * Método responsável por mostrar uma Mensagem
     *
     * @param title
     * @param text
     * @param activity
     ***/
    public static void showMessage(String title, String text, Activity activity) {
        AlertDialog.Builder message = new AlertDialog.Builder(activity);
        message.setTitle(title);
        message.setMessage(text);
        message.setNeutralButton("OK", null);
        message.show();
    }

    /***
     * Método responsável por mostrar uma Mensagem de opções "SIM" E "NÃO"
     *
     * @param title
     * @param text
     * @param activity
     * @param listenerPositiveButton
     * @param listenerNegativeButton
     **/
    public static void showMessageWithQuestion(String title, String text, Activity activity, DialogInterface.OnClickListener listenerPositiveButton, DialogInterface.OnClickListener listenerNegativeButton) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle(title);
        alertDialog.setMessage(text);
        alertDialog.setPositiveButton("Sim", listenerPositiveButton);
        alertDialog.setNegativeButton("Não", listenerNegativeButton);
        alertDialog.show();
    }

    /***
     * Método responsável por retornar um Date por extenso
     *
     * @param data
     * @return String
     ***/
    @SuppressLint("SimpleDateFormat")
    public static String getDataPorExtenso(Date data) {
        SimpleDateFormat dfmt = new SimpleDateFormat("d 'de' MMMM 'de' yyyy");
        return dfmt.format(data);
    }

    /***
     * Método responsável por retornar a data atual
     *
     * @return String
     ***/
    public static String getDate() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();

        return dateFormat.format(date);
    }

    /***
     * Método responsável por arredondar um valor de acordo com uma escala
     *
     * @param valor
     * @param escala
     * @return double
     ***/
    public static double arrendonda(double valor, int escala) {
        double d = valor;
        BigDecimal bd = new BigDecimal(d).setScale(escala,
                RoundingMode.HALF_EVEN);
        return (bd.doubleValue());
    }

    /***
     * Método responsável por receber uma String e retornar apenas os seus valores numéricos
     *
     * @param valor
     * @return String
     ***/
    public static String soNumeros(String valor) {

        StringBuffer sb = new StringBuffer();

        char[] caracteres = valor.toCharArray();

        for (Character caracter : caracteres) {
            if (Character.isDigit(caracter)) {
                sb.append(caracter);
            }
        }

        return sb.toString();
    }
}
