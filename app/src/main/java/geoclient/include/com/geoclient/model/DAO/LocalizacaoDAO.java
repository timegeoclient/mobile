package geoclient.include.com.geoclient.model.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import geoclient.include.com.geoclient.model.Localizacao;
import geoclient.include.com.geoclient.view.Principal;

/**
 * Created by João Filho on 21/10/2016.
 */

public class LocalizacaoDAO {

    /***
     * Banco de dados a ser manipulado
     ***/
    SQLiteDatabase bancoDados;
    /***
     * Flag para o nome da tabela: <b>Localizacao</b>
     ***/
    private final String TABELA = "LOCALIZACAO";
    /***
     * Flags dos campos da Tabela: <b>Localizacao</b>
     ***/
    private final String[] CAMPOS = Localizacao.colunasLocalizacao;

    /***
     * Método construtor
     ***/
    public LocalizacaoDAO(SQLiteDatabase bancoDados) {
        this.bancoDados = bancoDados;
    }

    /***
     * Localizar a Localização pelo codigo
     * localizado
     *
     * @param id
     ***/
    public Localizacao getLocalizacao(int id) {
        Cursor cursor = Principal.conexaoBanco.getCursor(CAMPOS, TABELA, Localizacao.ID_LOCA + " = " + String.valueOf(id), null, Localizacao.ID_LOCA);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            ColaboradorDAO CDAO = new ColaboradorDAO(this.bancoDados);
            Localizacao l = new Localizacao();

            l.setId(cursor.getInt(cursor.getColumnIndex(Localizacao.ID_LOCA)));
            l.setBairro(cursor.getString(cursor.getColumnIndex(Localizacao.BAIRRO_CLIENTE)));
            l.setCep(cursor.getString(cursor.getColumnIndex(Localizacao.CEP_CLIENTE)));
            l.setCidade(cursor.getString(cursor.getColumnIndex(Localizacao.CIDADE_CLIENTE)));
            l.setComplemento(cursor.getString(cursor.getColumnIndex(Localizacao.COMPLEMENTO_CLIENTE)));
            l.setEstado(cursor.getString(cursor.getColumnIndex(Localizacao.ESTADO_CLIENTE)));
            l.setLatitude(cursor.getString(cursor.getColumnIndex(Localizacao.LATITUDE_CLIENTE)));
            l.setLogradouro(cursor.getString(cursor.getColumnIndex(Localizacao.LOGRADORO_CLIENTE)));
            l.setLongitude(cursor.getString(cursor.getColumnIndex(Localizacao.LONGITUDE_CLIENTE)));
            l.setNome(cursor.getString(cursor.getColumnIndex(Localizacao.NOME_CLIENTE)));
            l.setNumero(cursor.getString(cursor.getColumnIndex(Localizacao.NUMERO_CLIENTE)));
            l.setPrecisao(cursor.getString(cursor.getColumnIndex(Localizacao.PRECISAO_LOCA)));
            //l.setRua(cursor.getString(cursor.getColumnIndex(Localizacao.)));
            l.setNumeroSatelitesAtivos(cursor.getInt(cursor.getColumnIndex(Localizacao.SATELITES_ATIVOS)));
            l.setStatus(cursor.getString(cursor.getColumnIndex(Localizacao.STATUS_VISITA)));
            //l.setEmpresa(cursor.getString(cursor.getColumnIndex(Localizacao.CIDADE_CLIENTE)));
            //l.setVisitas(cursor.getString(cursor.getColumnIndex(Localizacao.CIDADE_CLIENTE)));
            l.setColaborador(CDAO.getColaborador(cursor.getInt(cursor.getColumnIndex(Localizacao.ID_COLAB))));

            return l;
        } else {
            return null;
        }

    }

    /***
     * Salvar localização no banco de dados
     *
     * @param localizacao
     ***/
    public void salvarLocalizacao(Localizacao localizacao) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Localizacao.ID_LOCA, localizacao.getId());
        contentValues.put(Localizacao.SATELITES_ATIVOS, localizacao.getNumeroSatelitesAtivos());
        contentValues.put(Localizacao.PRECISAO_LOCA, localizacao.getPrecisao());
        contentValues.put(Localizacao.NOME_CLIENTE, localizacao.getNome());
        contentValues.put(Localizacao.LONGITUDE_CLIENTE, localizacao.getLongitude());
        contentValues.put(Localizacao.LATITUDE_CLIENTE, localizacao.getLatitude());
        contentValues.put(Localizacao.LOGRADORO_CLIENTE, localizacao.getLogradouro());
        contentValues.put(Localizacao.NUMERO_CLIENTE, localizacao.getNumero());
        contentValues.put(Localizacao.COMPLEMENTO_CLIENTE, localizacao.getComplemento());
        contentValues.put(Localizacao.BAIRRO_CLIENTE, localizacao.getBairro());
        contentValues.put(Localizacao.CEP_CLIENTE, localizacao.getCep());
        contentValues.put(Localizacao.CIDADE_CLIENTE, localizacao.getCidade());
        contentValues.put(Localizacao.ESTADO_CLIENTE, localizacao.getEstado());
        contentValues.put(Localizacao.ID_EMPRESA, localizacao.getEmpresa().getId());
        contentValues.put(Localizacao.ID_COLAB, localizacao.getColaborador().getId());
        contentValues.put(Localizacao.STATUS_VISITA, localizacao.getStatus());

        bancoDados.insert(TABELA, null, contentValues);
    }
}
