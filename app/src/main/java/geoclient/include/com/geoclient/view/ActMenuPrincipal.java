package geoclient.include.com.geoclient.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import geoclient.include.com.geoclient.R;

public class ActMenuPrincipal extends AppCompatActivity {

    Button btnListarClientes, btnSincronizacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_menu_principal);

        /***Vinculando Objetos aos componentes da View***/
        btnListarClientes = (Button) findViewById(R.id.actMenuPrincipal_btnListarClientes);
        btnSincronizacao = (Button) findViewById(R.id.actMenuPrincipal_btnSincronizacao);

        /*** OnClick do botão btnListarClientes ***/
        btnListarClientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActMenuPrincipal.this, ActListarVisitas.class);
                startActivity(i);
            }
        });

        /*** OnClick do botão btnSincronizacao ***/
        btnSincronizacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActMenuPrincipal.this, ActSincronizacao.class);
                startActivity(i);
            }
        });
    }
}
