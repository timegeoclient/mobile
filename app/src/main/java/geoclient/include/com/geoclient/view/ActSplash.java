package geoclient.include.com.geoclient.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import geoclient.include.com.geoclient.R;

public class ActSplash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_splash);
    }
}
