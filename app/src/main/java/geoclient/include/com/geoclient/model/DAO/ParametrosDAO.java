package geoclient.include.com.geoclient.model.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import geoclient.include.com.geoclient.model.Parametros;
import geoclient.include.com.geoclient.view.Principal;

/**
 * Created by João Filho on 22/10/2016.
 */

public class ParametrosDAO {
    /***
     * Banco de dados a ser manipulado
     ***/
    SQLiteDatabase bancoDados;
    /***
     * Flag para o nome da tabela: <b>Parametros</b>
     ***/
    private final String TABELA = "PARAMETROS";
    /***
     * Flags dos campos da Tabela: <b>Parametros</b>
     ***/
    private final String[] CAMPOS = Parametros.colunasParametros;

    /***
     * Método construtor
     ***/
    public ParametrosDAO(SQLiteDatabase bancoDados) {
        this.bancoDados = bancoDados;
    }

    /***
     * Salvar parametro no banco de dados
     *
     * @param p
     ***/
    public void salvarParametro(int cod_par, Parametros p) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Parametros.USUARIO_SALVO, p.getUsuario_salvo());
        contentValues.put(Parametros.SENHA_SALVA, p.getSenha_salva());

        bancoDados.update(TABELA, contentValues, Parametros.COD_PAR + " = 1", null);
    }

    /***
     * Retornar parametro
     *
     * @return
     ***/
    public Parametros getParametros() {
        Cursor cursor = Principal.conexaoBanco.getCursor(CAMPOS, TABELA, Parametros.COD_PAR + " = 1", null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            Parametros p = new Parametros();

            p.setCod_par(cursor.getInt(cursor.getColumnIndex(Parametros.COD_PAR)));
            p.setUsuario_salvo(cursor.getString(cursor.getColumnIndex(Parametros.USUARIO_SALVO)));
            p.setSenha_salva(cursor.getString(cursor.getColumnIndex(Parametros.SENHA_SALVA)));

            return p;
        } else {
            return null;
        }
    }
}
